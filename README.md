# Javascript Test #

#### Test Description
* Create a html page with blank content
* When user click anywhere on the page then will display a random shape (circle or rectangle) with ranrom size at mouse clicked position.
* After the shape is generated then it will automatically move with random speed to a random edge of screen.

#### Technical requirement
* Use OOP javascript
* Use module pattern
* Use strict mode
* Use **AMD** loading is a plus

#### Instructions
Fork this repository. Create a new branch to do your work. When done create a pull request back to studio60-vietnam as a new branch.

If there are any questions, feel free to record any assumptions made.